package edu.northeastern.ledlab.dotmemorytask

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*


object DBUtils {
    private fun getDatabase(): FirebaseFirestore {

        return FirebaseFirestore.getInstance()
    }

    fun addScore(pid: String, meanSetSize: Double, time: Long, log: String) {
        val db = DBUtils.getDatabase()

        val currTime = Calendar.getInstance().time

        val score = HashMap<String, Any>()
        score["time"] = time
        score["meansetsize"] = meanSetSize
        score["log"] = log

        db.collection("users")
                .document(pid)
                .collection("scores")
                .document(currTime.toString())
                .set(score)
                .addOnSuccessListener { documentReference ->
                    Log.d("addScore::Success", "DocumentSnapshot successfully written!")
                }
                .addOnFailureListener { e ->
                    Log.w("addScore::Failure", "Error writing document", e)
                }
    }
}